# 
# Copyright (C) 2018, 2019, 2020   Free Software Foundation, Inc.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#

# This class attempts to fix many common errors in OSM names, primarily
# because it break navigation. This script is a bit ugly and brute force,
# but does the trick for me. It's still hard to filter out all bad data,
# but this gets close.
import logging
import string
import epdb
import re


class correct(object):
    def __init__(self):
        self.orig = ""
        self.value = ""
        self.modified = False
        self.dirshort = ("S", "E", "N", "W")
        self.dirlong = ("South", "East", "North", "West")
        self.abbrevs = ("Hwy", "Rd", "Ln", "Dr", "Cir", "Ave", "Pl", "Trl", "Ct", "Cr", "FR", 'Crk', 'St', 'Ter', 'Av', 'Wy', 'Ci', 'Hi', 'Pt', 'Blvd')
        self.fullname = ("Highway", "Road", "Lane", "Drive", "Circle", "Avenue", "Place", "Trail", "Court", "CR", "FS", 'Creek', 'Street', 'Terrace', 'Avenue', 'Way', 'Circle', 'Heights', 'Point', 'Boulevard')

    def alphaNumeric(self, value=""):
        self.orig = value
        self.value = value
        m = re.search("[a-zA-Z ']+$", value)
        if m is not None:
            number = value[0:len(value)-1]
            suffix = string.capwords(value[len(value)-1])
            self.value = str(number) + suffix
            if self.value != value:
                # print("MODIFIED%r to %r" % (value, self.value))
                self.modified = True
        else:
            # print("NO CHANGE, %r" % self.value)
            self.value = value
        return self.value

    def caps(self, value):
        # self.value = value.capitalization()
        self.value = string.capwords(value)
        self.value = re.sub('^Cr ', 'CR ', value)
        self.value = re.sub(' Cr ', ' CR ', value)
        self.value = re.sub('^Fs ', 'FS ', value)
        self.value = re.sub(' Fs ', ' FS ', value)
        self.value = re.sub('^Us ', 'US ', value)
        self.value = re.sub(' Us ', ' US ', value)
        self.value = re.sub('^Co ', 'CO ', value)
        self.value = re.sub(' Co ', ' CO ', value)
        self.value = re.sub('State Hwy ', 'CO ', value)
        self.value = re.sub('County Rd ', 'CR ', value)

        return self.value

    def abbreviation(self, value=""):
        self.orig = value
        self.value = value
        # Fix abbreviations for road type
        i = 0
        while i < len(self.abbrevs):
            pattern = " " + self.abbrevs[i] + "$"
            m = re.search(pattern, value, re.IGNORECASE)
            pattern = "^" + self.abbrevs[i] # FIXME: + " "
            n = re.search(pattern, value, re.IGNORECASE)
            if n is not None:
                m = n
            if m is not None:
                pattern = " " + self.abbrevs[i] + " "
                newvalue = value[0:m.start()]
                rest = ' ' + value[m.start() + len(self.abbrevs[i])+1:len(value)]
                self.value = newvalue + ' ' + self.fullname[i] + rest.rstrip(' ')
                self.modified = True
                break

            pattern = " " + self.abbrevs[i] + " "
            m = re.search(pattern, value, re.IGNORECASE)
            if m is not None:
                newvalue = value[0:m.start()]
                rest = ' ' + value[m.start() + len(self.abbrevs[i])+1:len(value)]
                self.value = newvalue + ' ' + self.fullname[i] + rest
                self.modified = True

            # These seem to be special cases
            pattern = " spur"
            m = re.search(pattern, value)
            if m is not None:
                self.value = string.capwords(self.value)
                self.modified = True

            pattern = " Mtn "
            m = re.search(pattern, value)
            if m is not None:
                self.value = self.value.replace(pattern, " Mountain ")
                self.modified = True

            # Look for a few weird Rd patterns
            pattern = " Rd[\) ]+"
            m = re.search(pattern, value, re.IGNORECASE)
            if m is not None:
                self.value = self.value.replace(" Rd", " Road")
                self.modified = True
                break
            i = i +1

        return self.value.lstrip()
    
    def compass(self, value):
        self.orig = value
        i = 0
        # Fix compass direction names
        if len(value) == 0:
            epdb.st()
        while i < len(self.dirshort):
            pattern = "^" + self.dirshort[i] + ' '
            m = re.search(pattern, value, re.IGNORECASE)
            if m is not None:
                newvalue = self.dirlong[i] + ' '
                newvalue += value[2:]
                self.value = newvalue
                return newvalue

            pattern = " %s$" % self.dirshort[i]
            m = re.search(pattern, value, re.IGNORECASE)
            if m is not None:
                newvalue = " " + self.dirlong[i]
                newvalue += value[2:]
                self.value = newvalue
                return newvalue

            pattern = " %s " % self.dirshort[i]
            m = re.search(pattern, value, re.IGNORECASE)
            if m is not None:
                newvalue = '' + self.dirlong[i] + ' '
                newvalue += value[2:]
                self.value = newvalue
                return newvalue
            i = i +1

        return self.value

    def ismodified(self):
        return self.modified

    def convert(self, value=""):
        m = re.search("^Road .*", value, re.IGNORECASE)
        if m is not None:
            value.replace('Road ', 'CR ')
        m = re.search("^County Road .*", value, re.IGNORECASE)
        if m is not None:
            value.replace('County Road ', 'CR ')

    def tiger(self, tiger=None):
        """Take a name from Tiger and normalize it"""
        # Tiger address strings break everything download
        # into separate fields, which need to be converted.
        # ex: '(2666,,Miner,St,,,"Idaho Springs",CO,80452,,)'
        # housenumber, direction, name, roadtype, city, County
        # State, zipcode.
        logging.debug("TIGER: %r" % tiger)

        fixed = None
        item = tiger.strip(')(').split(',')
        if len(item[0]) == 0:
            # no housenumber
            return None
        else:
            number = int(item[0])
        direction = ""          # optional field
        if item[1] == "W":
            direction = "West "
        elif item[1] == "E":
            direction = "East "
        elif item[1] == "S":
            direction = "South "
        elif item[1] == "N":
            direction = "North "
        if len(item[2]) == 0:
            # no street name
            return None
        roadtype = self.abbreviation(item[3])
        street = item[2].strip('"')
        logging.debug("STREET: %r, ROAD: %r" % (street, roadtype))
        # street = fix.compass(street)
        if roadtype == "State Hwy":
            street = "CO"
        elif roadtype == "Co Road":
            roadtype = ""
            direction = "CR "
            street = item[2]
        elif roadtype == "Usfs":
            street = "FS"
            road = number

        city = item[6].strip('"')
        full = "%s%s %s" %(direction, street, roadtype)
        addr = "%s %s %s, %s" % (number, street, roadtype, city)
        # print(addr)

        return number,full

    def parcel(self, full):
        """Parse the string we get from parcel data"""
        number = None
        street = None
        unit = None
        range = None
        several = None
        pattern = re.compile('[0-9, ]+')
        tmp = pattern.match(full)
        if tmp is not None:
            end = tmp.end()
            number = full[:end].strip()
            # an embedded space, comma, or hypen means it's a range
            if number.find(' ') >= 0 or full.find(',') > 0:
                # see if it's a range, not a number
                numbers = full[:end].split()
                #several = range(int(numbers[0]), int(numbers[1]))
                several = numbers
                number = None
            else:
                number = int(full[:end].strip())
        else:
            end = 0
            number = None

        pattern = re.compile('unit [0-9A-Za-z]+', re.IGNORECASE)
        tmp = pattern.match(full)
        if tmp is not None:
            index = full[:tmp.end()].strip()
            unit = full[index:]
            street = full[end:index].strip()
        else:
            street = full[end:].strip()
            unit = None

        return number,street,unit,several
