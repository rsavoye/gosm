#!/usr/bin/python3

# 
# Copyright (C) 2018, 2019, 2020   Free Software Foundation, Inc.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# 

import os
import sys
import getopt
import logging
import epdb
from sys import argv


class AppConfigEntry(object):
    def __init__(self, key, abbrev, help="", value=None):
        self.key = key
        self.abbreviation = abbrev
        self.help = help
        self.value = value

    def set(self, opt):
        if type(opt) == list:
            self.key = opt[0]
            self.abbreviation = opt[1]
            if len(opt) >= 3:
                self.help = opt[2]
            if len(opt) == 4:
                self.value = opt[3]
        elif type(opt) == AppConfigEntry:
            self.options[opt['key']] = opt

    def dump(self):
        print("Key: --%s" % self.key)
        print("    Abbrev: -%s" % self.abbreviation)
        print("    Help: %s" % self.help)
        print("    Value: %r" % self.value)


class AppConfig(object):
    """Config data for this program."""
    def __init__(self, config=dict()):
        #if len(sys.argv) <= 1:
        #    self.usage()

        #epdb.st()
        self.base = os.path.basename(sys.argv[0].replace(".py", ""))
        self.options = dict()
        # All programs use these two options
        self.options['verbose'] = AppConfigEntry("verbose", "v", "Increase verbosity", 0)
        # Help quits the program, so doesn't need a value
        self.options['help'] = AppConfigEntry("help", "h", "Display help", None)
        optlet = "v,h"
        optstr = ["verbose", "help"]

        # # See if we have a disk based config file
        # self.name = sys.argv[0].replace('.py', '')
        # self.base = os.path.basename(self.name)
        # if os.path.exists(self.name + ".conf"):
        #     file = open(self.name + ".conf", 'r')
        #     line = file.readline()
        #     while len(line )> 0:
        #         key = line.split('=')[0]
        #         # The value is a  collection of lists
        #         value = line.split('=')[1]
        #         self.options[key] = value.rstrip()
        #         line = file.readline()

        # parameters override config file
        if config:
            for key,values in config.items():
                # print("FIXME1; %s %s" % (values.key, values.abbreviation))
                self.options[key] = values
                if values.value:
                    tmp = ",%s:" % values.abbreviation
                elif type(values.value) == bool:
                    tmp = ",%s" % values.abbreviation
                else:
                    tmp = ",%s:" % values.abbreviation
                optlet += tmp
                optstr.append(values.key)

        optlet += optlet[1:]
        opts = dict()
        try:
            print( optlet, optstr)
            (opts, val) = getopt.getopt(sys.argv[1:], optlet, optstr)
        except getopt.GetoptError as e:
            logging.error('%r' % e)
            self.usage()

        for (opt, val) in opts:
            if opt == '--help' or opt == '-h':
                self.usage(self.base)
            if opt == '--verbose' or opt == '-v':
                self.options['verbose'].value += 1
                continue
            for key,value in self.options.items():
                # print("FIXME; %s %s %s" % (opt, value.key, value.abbreviation)
                if opt[2:] == value.key or opt[1:] == value.abbreviation:
                    if type(self.options[key].value) is int:
                        self.options[key].value = int(val)
                    elif type(self.options[key].value) is float:
                        self.options[key].value = float(val)
                    elif type(self.options[key].value) is bool:
                        self.options[key].value = True
                    else:
                        self.options[key].value = val


        if self.options['verbose'].value > 0:
            logging.basicConfig(filename=self.base + '.log',level=logging.DEBUG)
            root = logging.getLogger()
            root.setLevel(logging.DEBUG)

            ch = logging.StreamHandler(sys.stdout)
            ch.setLevel(logging.DEBUG)
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            ch.setFormatter(formatter)
            root.addHandler(ch)
 
    def get(self, opt):
        try:
            return self.options[opt].value
        except:
            return None

    def set(self, opt, value):
        self.options[opt] = value

    # Basic help message
    def usage(self, argv=None):
        base = os.path.basename(sys.argv[0])
        print(base)
        for i,j in self.options.items():
            print("  --%s(-%s)\t%s" % (j.key, j.abbreviation, j.help))

    # Basic help message
    def dump(self):
        for i,j in self.options.items():
            j.dump()

