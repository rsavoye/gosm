#!/usr/bin/python3

# 
# Copyright (C) 2018, 2019, 2020   Free Software Foundation, Inc.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# 

from dejagnu import DejaGnu
# sys.path.append("..")
from appconfig import AppConfig,AppConfigEntry

dj = DejaGnu()

infile = "foo"
outfile = "bar"
opts = dict()

opts["infile"] = AppConfigEntry("infile", "i", "The input data file", str)
opts["outfile"] = AppConfigEntry("outfile", "o", "The output data file", "foo")

print("Done...")
ap = AppConfig(opts)
if ap.get('infile') == 'infile':
    dj.passes("infile")
else:
    dj.fails("infile")

ap.dump()
