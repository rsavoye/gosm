#!/usr/bin/python3

# 
# Copyright (C) 2020   Free Software Foundation, Inc.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# 

import os
import sys
import re
import string
import logging
import getopt
import pdb
from lxml import etree
from lxml.etree import tostring
from sys import argv
sys.path.append(os.path.dirname(argv[0]) + '/osmpylib')
from osm import OsmFile


class myconfig(object):
    def __init__(self, argv=list()):
        # Read the config file to get our OSM credentials, if we have any
        file = os.getenv('HOME') + "/.gosmrc"
        try:
            gosmfile = open(file, 'r')
        except Exception as inst:
            logging.warning("Couldn't open %s for writing! not using OSM credentials" % file)
            return
         # Default values for user options
        self.options = dict()
        self.options['logging'] = True
        self.options['verbose'] = False
        self.options['infile'] = os.path.dirname(argv[0])
        self.options['outfile'] = "./out.osm"

        try:
            (opts, val) = getopt.getopt(argv[1:], "h,o:,i:,v,",
                ["help", "outfile", "infile", "verbose"])
        except getopt.GetoptError as e:
            logging.error('%r' % e)
            self.usage(argv)
            quit()

        for (opt, val) in opts:
            if opt == '--help' or opt == '-h':
                self.usage(argv)
            elif opt == "--outfile" or opt == '-o':
                self.options['outfile'] = val
            elif opt == "--infile" or opt == '-i':
                self.options['infile'] = val
            elif opt == "--verbose" or opt == '-v':
                self.options['verbose'] = True
                logging.basicConfig(filename='shp2map.log',level=logging.DEBUG)

    def get(self, opt):
        try:
            return self.options[opt]
        except Exception as inst:
            return False

    def dump(self):
        logging.info("Dumping config")
        for i, j in self.options.items():
            print("\t%s: %s" % (i, j))

    # Basic help message
    def usage(self, argv):
        print(argv[0] + ": options: ")
        print("""\t--help(-h)   Help
\t--outfile(-o)   Output file name
\t--infile(-i)    Input file name
\t--verbose(-v)   Enable verbosity
        """)
        quit()


dd = myconfig(argv)
dd.dump()
if len(argv) <= 2:
    dd.usage(argv)

# The logfile contains multiple runs, so add a useful delimiter
try:
    logging.info("-----------------------\nStarting: %r " % argv)
except:
    pass

# if verbose, dump to the terminal as well as the logfile.
if dd.get('verbose') == 1:
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    root.addHandler(ch)

#
#
#
infile = dd.get('infile')
outfile = dd.get('outfile')

osm = OsmFile(dd, outfile)
osm.header()

doc = etree.parse(infile)
for docit in doc.getiterator():
    print("TAG: %r" % docit.tag)
    node = dict()
    if docit.tag == "node":
        print("TAG: %r" % docit.tag)
        for elit in docit.getiterator():
            for key,value in elit.items():
                if key == 'k':
                    newkey = value
                if key == 'v':
                    node[newkey] = value
                else:
                    node[key] = value
#                    print(node)
    elif docit.tag == "way":
        print("TAG: %r" % docit.tag)
        for elit in docit.getiterator():
            for key,value in elit.items():
                if key == 'k':
                    newkey = value
                if key == 'v':
                    node[newkey] = value
#                    print(node)
    elif docit.tag == "error":
        continue
    elif docit.tag == "text":
        continue
    elif docit.tag == "location":
        continue
    elif docit.tag == "tag":
        continue
    elif docit.tag == "class":
        continue
    elif docit.tag == "classtext":
        continue

    print(node)
    if node and 'lat' in node and 'lon' in node:
        way = osm.createNode(node)
        osm.writeWay(way[0])

osm.footer()
